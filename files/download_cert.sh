ANCHORS_PATH="/etc/pki/ca-trust/source/anchors"
ESTALEIRO_MIRROR="mirror.estaleiro.serpro"
ESTALEIRO_REGISTRY="hub.estaleiro.serpro"

echo "Q" | openssl s_client -showcerts -servername "$ESTALEIRO_REGISTRY" -connect "$ESTALEIRO_REGISTRY":443 | openssl x509 > "$ANCHORS_PATH"/"$ESTALEIRO_REGISTRY".pem
echo "Q" | openssl s_client -showcerts -servername "$ESTALEIRO_MIRROR"/ca/ca.pem -connect "$ESTALEIRO_MIRROR":443 | openssl x509 > "$ANCHORS_PATH"/ca.pem
update-ca-trust

